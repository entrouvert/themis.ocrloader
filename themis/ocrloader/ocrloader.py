# -*- coding: utf-8 -*-

import datetime
import os
import logging

from DateTime import DateTime

from Products.CMFCore.utils import getToolByName
from Products.Five.browser import BrowserView
from plone.namedfile.file import NamedBlobFile

from zope.event import notify
from zope.lifecycleevent import ObjectAddedEvent, ObjectModifiedEvent


import transaction

import themis.config.utils

log = logging.getLogger('Plone')

class LoadFromOcr(BrowserView):
    output_path = '/mnt/gedimport'

    def get_folder(self, doctype, category_folder):
        portal = getToolByName(self.context, 'portal_url').getPortalObject()
        folder = portal
        path = category_folder or themis.config.utils.get_ocr_location(doctype)
        for part in path.split('/'):
            if not part:
                continue
            folder = getattr(folder, part)
        return folder

    def __call__(self):
        # output path is the directory where the OCR system uploads the files
        self.output_path = self.request.form.get('outputPath', self.output_path)
        portal = getToolByName(self.context, 'portal_url').getPortalObject()
        plone_utils = getToolByName(self.context, 'plone_utils')

        for base, dirnames, filenames in os.walk(self.output_path):
            for filename in filenames:
                if not filename.lower().endswith('.pdf'):
                    continue
                log.info('processing %s' % filename)
                try:
                    code_cat, number, date, time = filename.split('_')
                except ValueError:
                    log.warning('unknown file name format (%s)' % filename)
                    os.rename(os.path.join(base, filename),
                          os.path.join(base, filename + '.invalid'))
                    continue

                for doctype in ('incoming_mails', 'outgoing_mails',
                                'internal_documents', 'confidential_documents'):
                    try:
                        category, subcategory, category_folder = \
                            themis.config.utils.get_categories_from_ocr_code(code_cat, doctype)
                    except TypeError:
                        continue
                    break
                else:
                    log.warning('no suitable document type found for %s' % filename)
                    os.rename(os.path.join(base, filename),
                          os.path.join(base, filename + '.invalid.nodoctype'))
                    continue

                folder = self.get_folder(doctype, category_folder)

                ocr_date = datetime.datetime(
                                int(date[0:4]), int(date[4:6]), int(date[6:]),
                                int(time[:2]),int(time[2:4]),int(time[4:6]))

                if (category or subcategory):
                    ocr_title = u'%s %s du %s à %s' % (
                                (category or subcategory),
                                number,
                                ocr_date.strftime('%d/%m/%Y'),
                                ocr_date.strftime('%H:%M:%S'))
                    ocr_id = plone_utils.normalizeString(
                                u'%s %s du %s à %s' % (
                                (category or subcategory),
                                number,
                                ocr_date.strftime('%Y-%m-%d'),
                                ocr_date.strftime('%H-%M-%S')))
                else:
                    ocr_title = u'Document %s du %s à %s' % (
                                number,
                                ocr_date.strftime('%d/%m/%Y'),
                                ocr_date.strftime('%H:%M:%S'))
                    ocr_id = plone_utils.normalizeString(
                                u'Document %s du %s à %s' % (
                                number,
                                ocr_date.strftime('%Y-%m-%d'),
                                ocr_date.strftime('%H-%M-%S')))

                if hasattr(folder, ocr_id):
                    logging.warning('document id already exists (%s)' % filename)
                    os.rename(os.path.join(base, filename),
                          os.path.join(base, filename + '.invalid.already-exists'))
                    continue

                if category:
                    category = [category]
                else:
                    category = None

                if subcategory:
                    subcategory = [subcategory]
                else:
                    subcategory = None

                ocr_file = NamedBlobFile(file(os.path.join(base, filename)).read(),
                                filename=unicode(filename))

                if ocr_date:
                    ocr_date = datetime.date.fromordinal(ocr_date.toordinal())

                if doctype == 'incoming_mails':
                    factory = 'courrier_entrant'
                    kwargs = {'numero_courrier': number,
                              'date_reception': ocr_date,
                              'categorie_de_courrier': category,
                              'sous_categorie_de_courrier': subcategory}
                elif doctype == 'outgoing_mails':
                    factory = 'courrier_sortant'
                    kwargs = {'numero_courrier': number,
                              'date_envoi': ocr_date,
                              'categorie_de_courrier': category,
                              'sous_categorie_de_courrier': subcategory}
                elif doctype == 'internal_documents':
                    factory = 'document_interne'
                    kwargs = {'no_du_dossier': number,
                              'categorie': category}
                elif doctype == 'confidential_documents':
                    factory = 'document_confidentiel'
                    kwargs = {'no_du_dossier': number,
                              'categorie': category}
                else:
                    logging.warning('unknown document type (%s, %s)' % (doctype, filename))
                    continue

                logging.info('invoking factory (%s) for %s (%r)' % (factory, ocr_id, ocr_title))
                oid = folder.invokeFactory(factory, id=ocr_id, title=ocr_title,
                        fichier=ocr_file, **kwargs)

                logging.info('done, %s has been created' % ocr_id)

                transaction.savepoint() #optimistic=True)
                logging.info('done, transaction.savepoint')

                try:
                    os.rename(os.path.join(base, filename),
                          os.path.join(base, filename + '.processed'))
                except OSError as e:
                    logging.warning('failed to rename to processed (%s) (%r)' % (ocr_id, str(e)))
                logging.info('rename done for %s' % ocr_id)

                try:
                    object = folder._getOb(oid)
                except (AttributeError, KeyError):
                    logging.warning('failed to getattr object (%s)' % ocr_id)
                else:
                    notify(ObjectAddedEvent(object))
                    logging.info('notifying done for %s' % ocr_id)
                return 'OK'

        return 'OK'

